const xml2js = require('xml2js')

const hwgsteXMLtoJson = (sensorXMLData) => {
  // const ip = sensor['ip']
  // const path = `/values.xml?${id}
  // const sensorURL = "http://"+ip+pather;

  const json = new xml2js.Parser();

  json.parseString(sensorXMLData, (err, result) => {
    temp = -999.0
    higro = -999.0

    if (undefined != result['val:Root']['SenSet'][0]['Entry']) {
      if (result['val:Root']['SenSet'][0]['Entry'][0]) temp = result['val:Root']['SenSet'][0]['Entry'][0]['Value'][0];
      if (result['val:Root']['SenSet'][0]['Entry'][1]) higro = result['val:Root']['SenSet'][0]['Entry'][1]['Value'][0];
    }
  })

  const result = {
    T: parseFloat(temp),
    H: parseFloat(higro)
  }

  return result
}
