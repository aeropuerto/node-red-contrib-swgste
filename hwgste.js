const sensorDataProcessor = require('./hwgste-reader')
const sensorResponse = require('./test/response')

module.exports = function(RED) {
  function HWGSTENode(config) {
    RED.nodes.createNode(this, config)
    var node = this
    node.on('input', function(msg) {
      // 1.- obtener el XML de la sonda
      // 2.- Procesar el xml para obtener los datos de temperatura y humedad
      const data = sensorDataProcessor(sensorResponse)
      // 3.- Devolver JSON con temperatura y humedad
      node.send(data)
    })
  }
  RED.nodes.registerType("HWg-STE", HWGSTENode)
}